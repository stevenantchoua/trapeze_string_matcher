package com.trapeze.test.controller;

import com.trapeze.test.domain.User;
import com.trapeze.test.service.SearchUserService;
import com.trapeze.test.service.exception.IncorrectParameterSyntaxException;
import com.trapeze.test.service.utils.UserPrinterToJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class RestApiController {
	@Autowired
	SearchUserService service;

	@GetMapping(path = "/searchuser", produces = "application/json")
	public String getUsers(@RequestParam("vorname") String vorname,@RequestParam("nachname") String nachname,@RequestParam("wohnort") String wohnort,@RequestParam("haarfarbe") String haarfarbe,@RequestParam("alter")String alter) throws IOException {
		List<User> resultats = null;
		try {
			resultats = service.getList(convertEmptyStringToNull(vorname), convertEmptyStringToNull(nachname), convertEmptyStringToNull(alter), convertEmptyStringToNull(haarfarbe), convertEmptyStringToNull(wohnort));
		} catch (IncorrectParameterSyntaxException e) {
			return e.getMessage();
		}
		return resultats.isEmpty() ? "no user found for given parameter":UserPrinterToJson.convertUser2Json(resultats);
	}

	private String convertEmptyStringToNull(String text){
		return text.isEmpty()? null:text;
	}
}
