package com.trapeze.test.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk_user_id")
	@NonNull
	private Long pk_user_id;
	@NonNull
	private String vorname;
	@NonNull
	private String nachname;
	@NonNull
	private Long alter;
	@NonNull
	private String haarfarbe;
	@NonNull
	private String wohnort;
	private Long plz;
}
