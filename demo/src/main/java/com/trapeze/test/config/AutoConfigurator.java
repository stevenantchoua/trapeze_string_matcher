package com.trapeze.test.config;

import com.trapeze.test.repository.UserRepository;
import com.trapeze.test.service.SearUserServiceImpl;
import com.trapeze.test.service.SearchUserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class AutoConfigurator {
	@Bean
	@Primary
	SearchUserService createSearcUserService(UserRepository userRepository){
		return new SearUserServiceImpl(userRepository);
	}
}
