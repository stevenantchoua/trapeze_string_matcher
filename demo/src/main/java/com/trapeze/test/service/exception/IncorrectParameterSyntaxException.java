package com.trapeze.test.service.exception;

public class IncorrectParameterSyntaxException extends Exception{
	public IncorrectParameterSyntaxException(String message) {
		super(message);
	}
}
