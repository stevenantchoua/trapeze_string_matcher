package com.trapeze.test.service;

import com.trapeze.test.domain.User;
import com.trapeze.test.service.exception.IncorrectParameterSyntaxException;

import java.util.List;

public interface SearchUserService {
	String checkTextAndReturnMatchedTextFrom(String text, String regex) throws IncorrectParameterSyntaxException;
	List<User> getList(String vorname, String nachname, String alter, String haarfarbe, String wohnort) throws IncorrectParameterSyntaxException;
}
