package com.trapeze.test.service.utils;

public class MatcherUtils {

	private MatcherUtils(){
		throw new IllegalStateException("statische utils class muss nicht instanziert werden!");	}

	public static boolean hasWilcard(String glob) {
		boolean resultat = false;

		for (int i = 0; i < glob.length(); ++i) {
			final char c = glob.charAt(i);
			switch (c) {
				case '*':
				case '?':
				case '>':
				case '<':
				case '-':
					resultat = true;
					break;
				default:
					break;
			}
		}
		return resultat;
	}

	public static String extractWordAndWilcard(String glob) {
		StringBuilder out = new StringBuilder("");

		for (int i = 0; i < glob.length(); ++i) {
			final char c = glob.charAt(i);
			switch (c) {
				case '*':
					out.append('%');
					break;
				case '?':
					out.append('_');
					break;
				default:
					out.append(c);
			}
		}

		if (out.charAt(0) == '_' && out.charAt(out.length() - 1) != '_' && out.charAt(out.length() - 1) != '%') {
			out.append("%");
		}
		return out.toString();
	}

	public static Long extractAlterValueFrom(String glob) {
		StringBuilder out = new StringBuilder("");

		for (int i = 0; i < glob.length(); ++i) {
			final char c = glob.charAt(i);
			switch (c) {
				case '<':
				case '>':
				case '-':
					break;
				default:
					out.append(c);
			}
		}

		return Long.valueOf(out.toString());
	}

	public static String createQueryParameter(String text) {
		if (text == null || text.isEmpty()) {
			return "%%";
		} else if (hasWilcard(text)) {
			return extractWordAndWilcard(text);
		} else {
			return '%' + text + '%';
		}
	}
}
