package com.trapeze.test.service;

import com.trapeze.test.domain.User;
import com.trapeze.test.repository.UserRepository;
import com.trapeze.test.service.exception.IncorrectParameterSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.trapeze.test.service.utils.MatcherUtils.*;

@Service
public class SearUserServiceImpl implements SearchUserService {
	private UserRepository userRepository;
	private static final Logger logger = LoggerFactory.getLogger(SearUserServiceImpl.class);
	private static final String GREATER_SYMBOL = ">";
	private static final String LESS_SYMBOL = "<";
	private static final String MINUS_SYMBOL = "-";
	private  static final String LETTER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER = "[a-zA-Z?*]";
	private static final String NUMBER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER = "[0-9<>-]";

	public SearUserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<User> getList(String vorname, String nachname, String alter, String haarfarbe, String wohnort) throws IncorrectParameterSyntaxException {
		createExceptionIfAllParameterSetByNull(vorname, nachname, alter, haarfarbe, wohnort);

		List<User> resultats = new ArrayList<>();

		String vornameLocal = createQueryParameter(checkTextAndReturnMatchedTextFrom(vorname, LETTER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER));
		String nachnameLocal = createQueryParameter(checkTextAndReturnMatchedTextFrom(nachname, LETTER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER));
		String haarfarbeLocal = createQueryParameter(checkTextAndReturnMatchedTextFrom(haarfarbe, LETTER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER));
		String wohnortLocal = createQueryParameter(checkTextAndReturnMatchedTextFrom(wohnort, LETTER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER));
		String filteredAlter = checkTextAndReturnMatchedTextFrom(alter, NUMBER_REGEX_WITH_ALLOW_SPECIAL_CHARACTER);

		if (filteredAlter == null || !hasWilcard(filteredAlter)) {
			String alterLocal = createQueryParameter(filteredAlter);

			resultats = userRepository.findAllUser(vornameLocal, nachnameLocal, wohnortLocal, haarfarbeLocal, alterLocal);
		} else {
			if (filteredAlter.contains(GREATER_SYMBOL) && !filteredAlter.contains(LESS_SYMBOL) && !filteredAlter.contains(MINUS_SYMBOL)) {
				Long alterLocal = extractAlterValueFrom(filteredAlter);

				resultats = userRepository.findAllGreaterByAlter(vornameLocal, nachnameLocal, wohnortLocal, haarfarbeLocal, alterLocal);
			} else if (filteredAlter.contains(LESS_SYMBOL) && !filteredAlter.contains(GREATER_SYMBOL) && !filteredAlter.contains(MINUS_SYMBOL)) {
				Long alterLocal = extractAlterValueFrom(filteredAlter);

				resultats = userRepository.findAllLessByAlter(vornameLocal, nachnameLocal, wohnortLocal, haarfarbeLocal, alterLocal);
			} else if (filteredAlter.contains(MINUS_SYMBOL) && !filteredAlter.contains(GREATER_SYMBOL) && !filteredAlter.contains(LESS_SYMBOL)) {
				String[] extractedValue = alter.split(MINUS_SYMBOL);

				Long fromValue = Long.valueOf(extractedValue[0]);
				Long toValue = Long.valueOf(extractedValue[1]);

				resultats = userRepository.findAllBetweenAlter(vornameLocal, nachnameLocal, wohnortLocal, haarfarbeLocal, fromValue, toValue);
			}
		}
		return resultats;
	}

	private void createExceptionIfAllParameterSetByNull(String vorname, String nachname, String alter, String haarfarbe, String wohnort) throws IncorrectParameterSyntaxException {
		if (iAllParameterEqualToNull(vorname, nachname, alter, haarfarbe, wohnort)) {
			String msg = "Mindestens ein Parameter muss festgelegt werden !";
			logger.error(msg);
			throw new IncorrectParameterSyntaxException(msg);
		}
	}

	private boolean iAllParameterEqualToNull(String vorname, String nachname, String alter, String haarfarbe, String wohnort) {
		return (vorname == null && nachname == null && alter == null && haarfarbe == null && wohnort == null);
	}

	@Override
	public String checkTextAndReturnMatchedTextFrom(String text, String regex) throws IncorrectParameterSyntaxException {
		if (text != null && !text.isEmpty()) {
			StringBuilder matchedText = new StringBuilder("");
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);

			Matcher matcher = pattern.matcher(text);

			while (matcher.find()) {
				matchedText.append(matcher.group());
			}
			String result = matchedText.toString();

			createExceptionIfNotAllowWilcardPresent(result, text);
			return result;
		}
		return text;
	}

	private void createExceptionIfNotAllowWilcardPresent(String filterText, String originalText) throws IncorrectParameterSyntaxException {
		if (!filterText.equals(originalText)) {
			String msg = "Dieser Parameter " + originalText + " ist ein illegal Parameter! Erlaubt Special Characters sind: ?*<>-";
			throw new IncorrectParameterSyntaxException(msg);
		}
	}
}
