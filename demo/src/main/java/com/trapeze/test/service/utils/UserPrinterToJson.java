package com.trapeze.test.service.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trapeze.test.domain.User;

import java.io.IOException;
import java.util.List;

public class UserPrinterToJson {
	private UserPrinterToJson() {
		throw new IllegalStateException("statische utils class muss nicht instanziert werden!");
	}

	public static String convertUser2Json(List<User> user) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(user);
	}
}
