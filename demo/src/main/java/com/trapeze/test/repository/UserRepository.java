package com.trapeze.test.repository;

import com.trapeze.test.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	@Query(value = "SELECT u FROM User u WHERE upper(u.vorname) LIKE CONCAT('', UPPER( :vorname))  " +
			" AND upper(u.nachname) LIKE CONCAT('', UPPER( :nachname)) " +
			" AND upper(u.wohnort) LIKE CONCAT('', UPPER( :wohnort))" +
			" AND upper(u.haarfarbe) LIKE CONCAT('', UPPER( :haarfarbe))" +
			" AND upper(u.alter) LIKE CONCAT('', UPPER( :alter))")
	List<User> findAllUser(@Param("vorname") String vorname, @Param("nachname") String nachname, @Param("wohnort") String wohnort, @Param("haarfarbe") String haarfarbe, @Param("alter") String alter);

	@Query(value = "SELECT u FROM User u WHERE upper(u.vorname) LIKE CONCAT('', UPPER( :vorname))  " +
			" AND upper(u.nachname) LIKE CONCAT('', UPPER( :nachname)) " +
			" AND upper(u.wohnort) LIKE CONCAT('', UPPER( :wohnort))" +
			" AND upper(u.haarfarbe) LIKE CONCAT('', UPPER( :haarfarbe))" +
			" AND u.alter BETWEEN :fromValue AND :toValue")
	List<User> findAllBetweenAlter(@Param("vorname") String vorname, @Param("nachname") String nachname, @Param("wohnort") String wohnort, @Param("haarfarbe") String haarfarbe, @Param("fromValue") Long fromValue, @Param("toValue") Long toValue);

	@Query(value = "SELECT u FROM User u WHERE upper(u.vorname) LIKE CONCAT('', UPPER( :vorname))  " +
			" AND upper(u.nachname) LIKE CONCAT('', UPPER( :nachname)) " +
			" AND upper(u.wohnort) LIKE CONCAT('', UPPER( :wohnort))" +
			" AND upper(u.haarfarbe) LIKE CONCAT('', UPPER( :haarfarbe))" +
			" AND u.alter <:alter")
	List<User> findAllLessByAlter(@Param("vorname") String vorname, @Param("nachname") String nachname, @Param("wohnort") String wohnort, @Param("haarfarbe") String haarfarbe, @Param("alter") Long alter);

	@Query(value = "SELECT u FROM User u WHERE upper(u.vorname) LIKE CONCAT('', UPPER( :vorname))  " +
			" AND upper(u.nachname) LIKE CONCAT('', UPPER( :nachname)) " +
			" AND upper(u.wohnort) LIKE CONCAT('', UPPER( :wohnort))" +
			" AND upper(u.haarfarbe) LIKE CONCAT('', UPPER( :haarfarbe))" +
			" AND u.alter >:alter")
	List<User> findAllGreaterByAlter(@Param("vorname") String vorname, @Param("nachname") String nachname, @Param("wohnort") String wohnort, @Param("haarfarbe") String haarfarbe, @Param("alter") Long alter);
}
