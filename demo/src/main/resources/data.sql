--
-- Definition of table `user`
--
CREATE TABLE `user`
(
    pk_user_id int(11) NOT NULL AUTO_INCREMENT,
    vorname    varchar(50) NOT NULL DEFAULT '',
    `nachname`   varchar(50) NOT NULL DEFAULT '',
    `alter`      int(11) NOT NULL DEFAULT 0,
    `haarfarbe`  varchar(45) NOT NULL DEFAULT '',
    `wohnort`    varchar(45) NOT NULL DEFAULT '',
    plz int(5) NOT NULL ,
    PRIMARY KEY (`pk_user_id`)
);
--
-- Dumping data for table `user`
--
INSERT INTO `user` (`pk_user_id`, `vorname`, `nachname`, `alter`, `haarfarbe`, `wohnort`,plz)
VALUES (1, 'Martin', 'Einhoff', 56, 'braun', 'Lengfeld',64783),
       (2, 'Michael', 'Becker', 50, 'dunkelblond', 'Weiterstadt',47629),
       (3, 'Max', 'Mustermann', 34, 'schwarz', 'Darmstadt',78362),
       (4, 'Sabine', 'Musterfrau', 25, 'hellblond', 'Darmstadt',74293),
       (5, 'Gabi', 'Müller', 41, 'rot', 'Griesheim',12673),
       (6, 'Udo', 'Lindenberg', 75, 'grau', 'Hamburg',81776),
       (7, 'Thierry Steve', 'Nantchoua', 28, 'schwarz', 'Seckmauern',78362),
       (8, 'Poppei', 'Seemann', 101, 'kahl', 'Hanau',89722);
