package com.trapeze.test.utils;

import com.trapeze.test.domain.User;

public class UserProvider {

	public static User USER1 = User.builder()
			.pk_user_id(1L)
			.vorname("Martin")
			.nachname("Einhoff")
			.alter(56L)
			.haarfarbe("braun")
			.wohnort("Lengfeld")
			.plz(64783L)
			.build();


	public static User USER2 = User.builder()
			.pk_user_id(2L)
			.vorname("Michael")
			.nachname("Becker")
			.alter(50L)
			.haarfarbe("dunkelblond")
			.wohnort("Weiterstadt")
			.plz(47629L)
			.build();

	public static User USER3 = User.builder()
			.pk_user_id(3L)
			.vorname("Thierry Steve")
			.nachname("Nantchoua")
			.alter(28L)
			.haarfarbe("schwarz")
			.wohnort("Seckmauern")
			.plz(78362L)
			.build();

	public static User USER4 = User.builder()
			.pk_user_id(4L)
			.vorname("Max")
			.nachname("Mustermann")
			.alter(34L)
			.haarfarbe("schwarz")
			.wohnort("Darmstadt")
			.plz(78362L)
			.build();
}
