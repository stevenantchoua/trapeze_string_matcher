package com.trapeze.test.repository;

import com.trapeze.test.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static com.trapeze.test.utils.UserProvider.*;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class UserRepositoryTest {
	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void setUp(){
		assert(userRepository.findAll().size() == 8);
	}


	@Test
	void shouldGetAllUserThatContainGivenWordInNachname() {
		List<User> returnedUsers = userRepository.findAllUser("%%","%E%","%%","%%","%%");

		assertThat(returnedUsers).hasSize(7);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER1.getNachname());
		assertThat(returnedUsers.get(0).getVorname()).isEqualTo(USER1.getVorname());
		assertThat(returnedUsers.get(0).getAlter()).isEqualTo(USER1.getAlter());
		assertThat(returnedUsers.get(1).getNachname()).isEqualTo(USER2.getNachname());
		assertThat(returnedUsers.get(1).getVorname()).isEqualTo(USER2.getVorname());
		assertThat(returnedUsers.get(1).getAlter()).isEqualTo(USER2.getAlter());
	}

	@Test
	void shouldGetAllUserWhenAllParameterEqualPercent() {
		List<User> returnedUsers = userRepository.findAllUser("%%","%%","%%","%%","%%");

		assertThat(returnedUsers).hasSize(8);
	}

	@Test
	void shouldGetAllUserThatStartWithGivenWordInVorname() {
		List<User> returnedUsers = userRepository.findAllUser("Mi%","%","%","%","%");

		assertThat(returnedUsers).hasSize(1);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER2.getNachname());
		assertThat(returnedUsers.get(0).getVorname()).isEqualTo(USER2.getVorname());
		assertThat(returnedUsers.get(0).getAlter()).isEqualTo(USER2.getAlter());
	}

	@Test
	void shouldGetAllUserThatEndWithGivenWordInWohnort() {
		List<User> returnedUsers = userRepository.findAllUser("%%","%%","%eld","%%","%%");

		assertThat(returnedUsers).hasSize(1);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER1.getNachname());
		assertThat(returnedUsers.get(0).getVorname()).isEqualTo(USER1.getVorname());
		assertThat(returnedUsers.get(0).getAlter()).isEqualTo(USER1.getAlter());
	}

	@Test
	void shouldGetAllUserWithAgeEgalTo56() {
		List<User> returnedUsers = userRepository.findAllUser("%%","%%","%%","%%","%28%");

		assertThat(returnedUsers).hasSize(1);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER3.getNachname());
		assertThat(returnedUsers.get(0).getVorname()).isEqualTo(USER3.getVorname());
		assertThat(returnedUsers.get(0).getAlter()).isEqualTo(USER3.getAlter());
	}

	@Test
	void shouldGetAllUserThatRespectGivenCondition() {
		List<User> returnedUsers = userRepository.findAllUser("%art%","%off","%%","br%","%%");

		assertThat(returnedUsers).hasSize(1);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER1.getNachname());
		assertThat(returnedUsers.get(0).getVorname()).isEqualTo(USER1.getVorname());
		assertThat(returnedUsers.get(0).getAlter()).isEqualTo(USER1.getAlter());
	}

	@Test
	void shouldGetAllUserThatNameStartWithEveryWordWithAAsSecondWord() {
		List<User> returnedUsers = userRepository.findAllUser("_a%","%%","%%","%%","%%");

		assertThat(returnedUsers).hasSize(4);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER1.getNachname());
		assertThat(returnedUsers.get(1).getNachname()).isEqualTo(USER4.getNachname());
	}

	@Test
	void shouldGetAllUserWhereAgeLessThan50() {
		List<User> returnedUsers = userRepository.findAllLessByAlter("%%","%%","%%","%%",50L);

		assertThat(returnedUsers).hasSize(4);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER4.getNachname());
		assertThat(returnedUsers.get(3).getNachname()).isEqualTo(USER3.getNachname());
	}

	@Test
	void shouldGetAllUserWhereAgeGreaterThan50() {
		List<User> returnedUsers = userRepository.findAllGreaterByAlter("%%","%%","%%","%%",50L);

		assertThat(returnedUsers).hasSize(3);
		assertThat(returnedUsers.get(0).getNachname()).isEqualTo(USER1.getNachname());
	}
}