package com.trapeze.test.service;

import com.trapeze.test.domain.User;
import com.trapeze.test.repository.UserRepository;
import com.trapeze.test.service.exception.IncorrectParameterSyntaxException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.trapeze.test.utils.UserProvider.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SearUserServiceImplTest {
	@Mock
	UserRepository userRepository;
	SearchUserService searchUserService;

	@BeforeEach
	void setUp() {
		searchUserService = new SearUserServiceImpl(userRepository);
	}

	@Test
	void shouldGetExceptionWhenAllParameterAreEqualToNull(){
		String expectedMessage = "Mindestens ein Parameter muss festgelegt werden !";
		IncorrectParameterSyntaxException returnedException = assertThrows(IncorrectParameterSyntaxException.class, () -> {
			searchUserService.getList(null, null, null, null, null);
		});

		assertThat(expectedMessage).isEqualTo(returnedException.getMessage());
	}

	@Test
	void shouldGetExceptionWhenIllegalParameterPresent(){
		String expectedMessage = "Dieser Parameter ma> ist ein illegal Parameter! Erlaubt Special Characters sind: ?*<>-";
		IncorrectParameterSyntaxException returnedException = assertThrows(IncorrectParameterSyntaxException.class, () -> {
			searchUserService.getList("ma>",null,null,null,null);
		});

		assertThat(expectedMessage).isEqualTo(returnedException.getMessage());
	}

	@Test
	void shouldGetAllUsersThatContainTheGivenVornameWithoutWilcard() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllUser("%ma%","%%","%%","%%","%%")).thenReturn(Collections.singletonList(USER1));
		List<User> returnedResults = searchUserService.getList("ma", null, null, null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER1.getNachname());
	}

	@Test
	void shouldGetAllUsersThatContainVornameWithQuestionMarkBefore() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllUser("_ma%","%%","%%","%%","%%")).thenReturn(Collections.singletonList(USER1));
		List<User> returnedResults = searchUserService.getList("?ma", null, null, null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER1.getNachname());
		assertThat(returnedResults.get(0).getVorname()).isEqualTo(USER1.getVorname());
		assertThat(returnedResults.get(0).getWohnort()).isEqualTo(USER1.getWohnort());
		assertThat(returnedResults.get(0).getHaarfarbe()).isEqualTo(USER1.getHaarfarbe());
		assertThat(returnedResults.get(0).getAlter()).isEqualTo(USER1.getAlter());
	}

	@Test
	void shouldGetAllUsersThatStartWithBeInNachname() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllUser("%%","be%","%%","%%","%%")).thenReturn(Collections.singletonList(USER2));
		List<User> returnedResults = searchUserService.getList(null, "be*", null, null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER2.getNachname());
	}

	@Test
	void shouldGetAllUsersThatContentTchInNachname() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllUser("%%","%tch%","%%","%%","%%")).thenReturn(Collections.singletonList(USER3));
		List<User> returnedResults = searchUserService.getList(null, "*tch*", null, null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER3.getNachname());
	}

	@Test
	void shouldGetAllUserThatContentArInNachname() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllUser("%%","_ar%","%%","%%","%%")).thenReturn(Collections.singletonList(USER1));
		List<User> returnedResults = searchUserService.getList(null, "?ar*", null, null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER1.getNachname());
	}

	@Test
	void shouldGetAllUserThatContentErmAfter4WordsInNachname() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllUser("%%","____erm%","%%","%%","%%")).thenReturn(Collections.singletonList(USER4));
		List<User> returnedResults = searchUserService.getList(null, "????erm*", null, null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER4.getNachname());
	}

	@Test
	void shouldGetAllUserThatAgeGreaterThan50() throws IncorrectParameterSyntaxException {
		when(userRepository.findAllGreaterByAlter("%%","%%","%%","%%",50L)).thenReturn(Collections.singletonList(USER1));
		List<User> returnedResults = searchUserService.getList(null, null, ">50", null, null);

		assertThat(returnedResults).hasSize(1);
		assertThat(returnedResults.get(0).getNachname()).isEqualTo(USER1.getNachname());
	}
	@Test
	void shouldGetAllUserThatAgeLessThan50() throws IncorrectParameterSyntaxException {
		List<User> expectedResults = Arrays.asList(USER3, USER4);
		when(userRepository.findAllLessByAlter("%%","%%","%%","%%",50L)).thenReturn(expectedResults);
		List<User> returnedResults = searchUserService.getList(null, null, "<50", null, null);

		assertThat(returnedResults).hasSize(2);
		assertThat(expectedResults).containsExactlyElementsOf(expectedResults);
	}

	@Test
	void shouldGetAllUserThatAgeBetween20And50() throws IncorrectParameterSyntaxException {
		List<User> expectedResults = Arrays.asList(USER2, USER3, USER4);
		when(userRepository.findAllBetweenAlter("%%","%%","%%","%%",20L,50L)).thenReturn(expectedResults);
		List<User> returnedResults = searchUserService.getList(null, null, "20-50", null, null);

		assertThat(returnedResults).hasSize(3);
		assertThat(expectedResults).containsExactlyElementsOf(expectedResults);
	}
}